// Assignment Operators

	// Basic Assignment Operator (=)
    // The assignment operator adds the value of the right operand to a variable and assigns the result to the variable.

let assignmentNumber = 8;
console.log(assignmentNumber);

	// Addition Assignment Operator

	let totalNumber = assignmentNumber + 2;
	console.log("Result of addition assignment operator: " + totalNumber);

	// Arithmetic Operator (+, -, *, /, %)

	let x = 235;
	let y = 5;

	let sum = x + y;
	console.log("Result of addition operator: " + sum);

	let	difference = x - y;
	console.log("Result of subtraction operator: " + difference);

let	product = x * y;
	console.log("Result of multiplication operator: " + product);

let	quotient = y / x;
	console.log("Result of quotient operator: " + quotient);

	let remainder = y % x;
	console.log("Result of modulo operator: " + remainder);

	