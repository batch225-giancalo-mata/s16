// Multiple Operators and Parentheses (MDAS and PEMDAS)

/*- When multiple operators are applied in a single statement, it follows the PEMDAS (Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction) rule
            - The operations were done in the following order:
                1. 3 * 4 = 12
                2. 12 / 5 = 2.4
                3. 1 + 2 = 3
                4. 3 - 2.4 = 0.6*/

let mDas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mDas operation: " + mDas);

    //let mDas2 = 6 / 2 * (1 + 3);

    	/*
    		1. (1 + 2) = 3
    		2. 2 * 3 = 6
    		3. 6 / 6 = 1
    	*/

console.log("Result of mDas operation: " + mDas);
    //console.log("Result of mDas operation: " + mDas2);

let pemDas = 1 + (2 - 3) * (4 / 5);

		/*
			1. 4 / 5 = 0.8
			2. 2 - 3 = -1
			3. -1 * 0.8 = -0.8
			4
		*/

console.log(pemDas);


let minutesHour = 60;
let hoursDay = 24;
let daysWeek = 7;
let weeksMonth = 4;
let monthsYear = 12;
let daysYear = 365;

let	resultMinutes = minutesHour * hoursDay * daysYear;
console.log("Minutes in a year: " + resultMinutes);


let tempCelsius = 132;
let	resultFarenheit = (tempCelsius * 1.8000) + 32;
console.log(tempCelsius + " degrees Celsius equals " + resultFarenheit + " in Farenheit");


// Comparison Operator

// Equality Operator (==)

	/*
		- Checks whether the operands are equal / have the same content.
		- Note: = is for assignment operator
		- Note: == is for equality operator
		- Attempts to Convert and Compare operands of different data types.
		- True == 1 and False == 0
	*/

let juan = 'juan';

	console.log(1 == 1); // True
	console.log(1 == 2); // False
	console.log(1 == '1'); // True
	console.log(0 == false); // True
	//Compares two strings that are the same 
	console.log('juan' == 'juan'); // True
	//Compares a string with the variable "juan" declared above 
	console.log('juan' == juan); // True

	// Inequality operator
	/*
		- Checks whether the operands are not equal/have different content.
		- Attempts to CONVERT AND COMPARE operands of different data types.
	*/

	console.log(1 != 1); // False
	console.log(1 != 2); // True
	console.log(1 != '1'); // False
	console.log(0 != false); // False
	console.log('juan' != 'juan'); // False
	console.log('juan' != juan); // False

	// Strict Equality Operator (===)
/* 
            - Checks whether the operands are equal/have the same content
            - Also COMPARES the data types of 2 values
            - JavaScript is a loosely typed language meaning that values of different data types can be stored in variables
            - In combination with type coercion, this sometimes creates problems within our code (e.g. Java, Typescript)
            - Some programming languages require the developers to explicitly define the data type stored in variables to prevent this from happening
            - Strict equality operators are better to use in most cases to ensure that data types provided are correct
        */

	console.log(1 === 1);
	console.log(1 === 2);
	console.log(1 === '1');
	console.log(0 === false);
	console.log('juan' === 'juan');
	console.log('juan' === juan);


	// Relational Operators
	//Some comparison operators check whether one value is greater or less than to the other value.

	let a = 50;
	let b = 65;

        //GT or Greater Than operator ( > )
        let isGreaterThan = a > b; // 50 > 65
        //LT or Less Than operator ( < )
        let isLessThan = a < b; // 50 < 65
        //GTE or Greater Than Or Equal operator ( >= ) 
        let isGTorEqual = a >= b; // 50 >= 65
        //LTE or Less Than Or Equal operator ( <= ) 
        let isLTorEqual = a <= b; // 50 <= 65

        //Like our equality comparison operators, relational operators also return boolean which we can save in a variable or use in a conditional statement.
        console.log(isGreaterThan);
        console.log(isLessThan);
        console.log(isGTorEqual);
        console.log(isLTorEqual);

        // Logical Operators (&&, ||, !)

        let isLegalAge = true;
        let isRegistered = false;

        // Logical Operator (&& - Double Ampersand - And Operator) (|| = Or) (!=Not)
        // Return true if all operands are true
        // Note True = 1 false = 0

        // 1 && 1 = true
        // 1 && 0 = false
        // 0 && 1 = false
        // 0 && 0 = false

        let allRequirements = isLegalAge && isRegistered;
    //   console.log("Result of logical AND Operator " + allRequirementsMet);

        // Logical Or Operator (|| - Double Pipe)
    // Returns true if one of the operands are true 

    // 1 || 1 = true
    // 1 || 0 = true
    // 0 || 1 = true
    // 0 || 0 = false

        let someRequirementsMet = isLegalAge || isRegistered; 

        console.log("Result of logical OR Operator: " + someRequirementsMet);
    // Passing is 1
    //   1. ques
    //   2. question
        let securityQuestion = 1 || 2

    //    password
    //    confirm password


	// Logical Not Operator (! - Exclamation Point)
    // Returns the opposite value 
        let someRequirementsNotMet = !isRegistered;
        console.log("Result of logical NOT Operator: " + someRequirementsNotMet);



/*
	4a. Given the values below, identify if the values of the following variable are divisible by 8.
	   -Use a modulo operator to identify the divisibility of the number to 8.
	   -Save the result of the operation in an appropriately named variable.
	   -Log the value of the remainder in the console.
	   -Using the strict equality operator, check if the remainder is equal to 0. Save the returned value of the comparison in a variable called isDivisibleBy8
	   -Log a message in the console if num7 is divisible by 8.
	   -Log the value of isDivisibleBy8 in the console.

*/
	let num7 = 165;
	let num77 = 8;
	let remainder = num7 % num77;
	console.log("Is num7 divisible by 8?")
	console.log("remainder = " + remainder);
	//Log the value of the remainder in the console.
	console.log("Is num7 divisible by 8?" + remainder == 0);
	//Log the value of isDivisibleBy8 in the console.


/*
	4b. Given the values below, identify if the values of the following variable are divisible by 4.
	   -Use a modulo operator to identify the divisibility of the number to 4.
	   -Save the result of the operation in an appropriately named variable.
	   -Log the value of the remainder in the console.
	   -Using the strict equality operator, check if the remainder is equal to 0. Save the returned value of the comparison in a variable called isDivisibleBy4
	   -Log a message in the console if num8 is divisible by 4.
	   -Log the value of isDivisibleBy4 in the console.

*/
	let num8 = 348;
	let num88 = 4;
	let remainder2 = num8 % num88;
	console.log("Is num8 divisible by 4?")
	console.log("remainder2 = " + remainder2);
	//Log the value of the remainder in the console.
	console.log("Is num8 divisible by 4?" + remainder2 != 0);
	//Log the value of isDivisibleBy4 in the console.
